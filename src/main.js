import Vue from 'vue'
import App from './App.vue'
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
import VueRouter from 'vue-router'
import {Rutas} from './Rutas/Rutas'

Vue.use(VueRouter);
const enrutador = new VueRouter({
  routes: Rutas
})

new Vue({
  el: '#app',
  routas:Rutas,
  render: h => h(App)
})   

